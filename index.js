const hamburgerIcon = document.querySelector(".breadcrumb");
const divNavLinks = document.querySelector(".nav-down");

hamburgerIcon.addEventListener("click", () => {
    if (divNavLinks.getAttribute("class") == "hidden-nav"){
        divNavLinks.setAttribute("class", "show-nav");
    } 
    else{
        divNavLinks.setAttribute("class", "hidden-nav");
    } 
});
